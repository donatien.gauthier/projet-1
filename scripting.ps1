Write-Host " Bonjour moi c'est Donatien, Bienvenue sur mon scripting" -ForegroundColor Red

Write-Host " " -ForegroundColor Green

Write-Host " Nom de la Machine:" -ForegroundColor Green 
hostname  # nom de la machine

Write-Host " "

Write-Host " IP :" -ForegroundColor Green 
((ipconfig | findstr [0-9]..)[6]).Split()[-1] # IP

Write-Host " "

Write-Host " OS:" -ForegroundColor Green 
(Get-WmiObject Win32_OperatingSystem).Version    # version OS
(Get-WmiObject -class Win32_OperatingSystem).Caption 

Write-Host " "

Write-Host " Date et heure d'allumage:" -ForegroundColor Green 
net stats workstation | findstr "depuis"  # date et heure d'allumage

Write-Host " "

$ram_max = ((Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory/1GB)
$ram_libre = ((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory/1MB)
$ram_utilis = $ram_max - $ram_libre
Write-Host " RAM Max:" -ForegroundColor Green 
$ram_max
Write-Host " RAM Libre:" -ForegroundColor Green
$ram_libre
Write-Host " RAM Utilis:" -ForegroundColor Green
$ram_utilis
Write-Host " "


$diskSpace = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='C:'" | Measure-Object Size -Sum).sum /1gb)
$diskFree = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='C:'" | Measure-Object FreeSpace -Sum).sum /1gb)
$diskUsed = [math]::Round($diskSpace-$diskFree)
$diskSpace1 = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='D:'" | Measure-Object Size -Sum).sum /1gb)
$diskFree1 = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='D:'" | Measure-Object FreeSpace -Sum).sum /1gb)
$diskUsed1 = [math]::Round($diskSpace1-$diskFree1)
$diskSpace2 = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='E:'" | Measure-Object Size -Sum).sum /1gb)
$diskFree2 = [math]::Round((Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='E:'" | Measure-Object FreeSpace -Sum).sum /1gb)
$diskUsed2 = [math]::Round($diskSpace2-$diskFree2)
Write-Host " Total Memoire:" -ForegroundColor Green
Get-WmiObject Win32_LogicalDisk | findstr "DeviceID" 
$diskSpace
$diskSpace1
$diskSpace2

Write-Host " Espace Libre:" -ForegroundColor Green
$diskFree
$diskFree1
$diskFree2


Write-Host " Memoire utilis:" -ForegroundColor Green
$diskUsed
$diskUsed1
$diskUsed2

Write-Host " "




