$param = $args[0]
$time = $args[1]

if ($param -eq 'shutdown') {
   if ($time) {
        Write-Host 'Your computer will shutdown in'$time 'second(s)'
        Start-Sleep -Seconds $time
        shutdown
   }     
}
elseif ($param -eq 'lock') {
   if ($time) {
        Write-Host 'Your computer will be lock in'$time 'second(s)' 
        Start-Sleep -Seconds $time
        rundll32.exe user32.dll,LockWorkStation        
   }
}