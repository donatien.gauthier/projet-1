# Maîtrise de poste - day 1

Self-footprinting
=================

Host OS
-------
```
PS C:\Users\donat\ynov-TP\projet-1> hostname 
LAPTOP-H79VQEE6
```

```
PS C:\Users\donat\ynov-TP\projet-1> systeminfo                                                                                         
Nom de l’hôte:                              LAPTOP-H79VQEE6
Nom du système d’exploitation:              Microsoft Windows 10 Famille
Version du système:                         10.0.18362 N/A version 18362
[...]
Processeur(s):                              1 processeur(s) installé(s).
                                            [01] : Intel64 Family 6 Model 142 Stepping 10 GenuineIntel ~1600 MHz
[...]                                            
Mémoire physique totale:                    8 071 Mo
[...]
```
```
PS C:\Users\donat\ynov-TP\projet-1> Get-CimInstance win32_physicalmemory | Format-Table Manufacturer, PartNumber                       
Manufacturer PartNumber
------------ ----------
Samsung      M471A1K43BB1-CRC

```
Devices
-------
```
PS C:\Users\donat\ynov-TP\projet-1> wmic
wmic:root\cli>cpu get Name
Name
Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz
C'est un huitième génération 
```
```
PS C:\Users\donat\ynov-TP\projet-1> wmic
wmic:root\cli>cpu get Numberofcores
NumberOfCores
4
```
```
wmic:root\cli>quit
PS C:\Users\donat\ynov-TP\projet-1> Get-CimInstance Win32_PointingDevice

[...]
Caption                     : Synaptics SMBus TouchPad
Description                 : Synaptics SMBus TouchPad
InstallDate                 :
Name                        : Synaptics SMBus TouchPad
[...]
```
```
PS C:\Users\donat\ynov-TP\projet-1> Get-WmiObject Win32_VideoController

[...]
Caption                      : Intel(R) UHD Graphics 620
[...]
Caption                      : NVIDIA GeForce GTX 1050
[...]
VideoProcessor               : GeForce GTX 1050
[...]
```
```
PS C:\Users\donat\ynov-TP\projet-1> Get-Disk

Number Friendly Name                Serial Number                    HealthStatus         OperationalStatus      Total Size Partition
                                                                                                                            Style
------ -------------                -------------                    ------------         -----------------      ---------- ----------
1      SAMSUNG MZNLN128HAHQ-000H1   S3T8NE0K530445                   Healthy              Online                  119.24 GB GPT
0      ST1000LM035-1RK172           WL193332                         Healthy              Online                  931.51 GB GPT

```
```
PS C:\Users\donat\ynov-TP\projet-1> Get-Partition                                                                                      

   DiskPath : \\?\scsi#disk&ven_samsung&prod_mznln128hahq-000#4&21bf9b16&0&000200#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                                Size Type
---------------  ----------- ------                                                ---- ----
1                            1048576                                             260 MB System
2                            273678336                                            16 MB Reserved
3                C           290455552                                        118.01 GB Basic
4                            127001427968                                        980 MB Recovery


   DiskPath : \\?\scsi#disk&ven_st1000lm&prod_035-1rk172#4&21bf9b16&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                                Size Type
---------------  ----------- ------                                                ---- ----
1                D           1048576                                             917 GB Basic
2                E           984627544064                                      14.51 GB Basic
```

Users
-----
```
PS C:\Users\donat\ynov-TP\projet-1> Get-LocalUser                                                                                      
Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
DefaultAccount     False   Compte utilisateur géré par le système.
defaultuser100001  True
donat              True
Invité             False   Compte d’utilisateur invité
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
```
Processus
--------
```
PS C:\Users\donat\ynov-TP\projet-1> tasklist                                                                                           
Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
========================= ======== ================ =========== ============
svchost.exe                  31712 Console                   14    11 224 Ko
dllhost.exe                  24852 Console                   14     3 464 Ko
SystemSettings.exe           30372 Console                   14       452 Ko
conhost.exe                  27832 Console                   14     7 272 Ko
msedge.exe                   28372 Console                   14   100 112 Ko
```
pas fini


Network
-------
```
PS C:\Users\donat\ynov-TP\projet-1> Get-NetAdapter | fl Name, InterfaceIndex                                                           

Name           : Connexion réseau Bluetooth
InterfaceIndex : 13

Name           : Wi-Fi
InterfaceIndex : 9

Name           : Ethernet
InterfaceIndex : 4
```
```
PS C:\WINDOWS\system32> netstat -p tcp -n -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    10.33.2.184:50981      40.67.251.132:443      ESTABLISHED
 [OneDrive.exe]
  TCP    10.33.2.184:51078      52.113.194.132:443     ESTABLISHED
 [commsapps.exe]
  TCP    10.33.2.184:51411      51.141.8.249:443       ESTABLISHED
 [smartscreen.exe]
  TCP    10.33.2.184:51830      40.67.251.132:443      ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:51834      162.159.133.234:443    ESTABLISHED
 [Discord.exe]
  TCP    10.33.2.184:51859      40.67.251.132:443      ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    10.33.2.184:51893      52.44.229.184:443      ESTABLISHED
 [EpicGamesLauncher.exe]
  TCP    10.33.2.184:52073      13.113.49.218:443      ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52156      185.199.111.154:443    ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52166      104.244.42.200:443     ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52168      151.101.0.84:443       ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52182      104.18.169.19:443      ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52185      104.18.169.19:443      ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52186      199.232.194.49:443     ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52197      104.16.86.20:443       ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52219      151.101.193.69:443     ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52221      151.101.65.69:443      ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52222      104.16.29.34:443       ESTABLISHED
 [...]
 [EpicGamesLauncher.exe]
  TCP    10.33.2.184:52232      13.107.21.200:443      ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.184:52233      13.107.18.11:443       ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.184:52234      40.90.23.206:443       ESTABLISHED
  wlidsvc
 [svchost.exe]
  TCP    10.33.2.184:52235      3.213.18.157:443       ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52236      3.213.18.157:443       ESTABLISHED
 [msedge.exe]
  TCP    10.33.2.184:52237      13.107.246.254:443     ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.184:52238      13.107.4.254:443       ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.184:52239      93.184.220.29:80       ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.184:52240      13.107.246.10:443      ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.184:52241      204.79.197.222:443     ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.184:52242      10.33.2.42:7680        SYN_SENT
 ```
Scripting
=========
